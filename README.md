# Console

# Documentation
Core js files provide a js-based shell environment, allowing users to interact with a variety of `Commands`, which are modular.

## Defining your own command
Creating a command is a two-step process:
* Create a new js file in `/js/` and instantiate a new Object for your Command. Commands are JS objects with three attributes:
 * `name`: the command a user must type to launch your application (_eg. ping_)
 * `description`: a description of the command. HTML is supported here, so feel free to use `<br/>`s and give usage instructions. This appears when the `help` command is given
 * `execute`: a function definition of the form `function(input,output)`, with the attributes:
  * `input` an array of strings representing the user's input, separated by spaces. (_eg. "man 3 fopen" resolves to ["man","3","fopen"]_)
  * `output` a convinience function for writing a single line to the console. (_eg. `output('hello world');`_)
 * `autocomplete`: (**optional**) A function for autocompleting user queries of the form `autocomplete(input)`. Input is an array of strings, similar to the execute function. If a return value is provided, the users **entire** input will be replaced with this value. Eg `input=['hello','wor']` should return "hello world".
* At the end of your JS file, call `Modules.register(your-object)`

_Change: command js files no longer have to be manually included_

## Example

Here's an example Command, for returning 'Polo' when the user types 'marco':
```javascript
var Marco = {
  name:'marco',
  description:'An example command<br/>Usage: marco [name]',
  execute:function(input,output){
    var reply = "Polo"
    output(reply);
  }
}

Commands.register(Marco);
```

A slightly more complex execute function checks if the user has specified a name as an argument, and greets them:

```javascript
function(input,output){
    var reply = "Polo"
    if(input.length>1){
      reply += ', '+input[1];
    }
    reply += '!';
    output(reply);
  }
```

To handle input at a module level, define the `handleInput` and return `true` when handling the input parameter. For example here is a module that shows an alert when the star button is pressed:

```javascript
var Marco = {
  name:'marco',
  description:'An example command<br/>Usage: marco [name]',
  execute:function(input,output){
    output("Marco Polo!");
  },
  handleInput:function(keyCode) {
      var keyMultiplyCode = 106;
      if(keyCode == keyMultiplyCode) {
        alert('Marco is a star!');
        return true;
      }
      return false;
    }
  }
}
```

## System methods
The following objects are globally defined:
* Display - Responsible for the console output window
* UserInput - Responsible for the last line of the console, where the user is typing
* History - Responsible for keeping track of the user's most recent commands
* Commands - Responsible for the handover between shell and command

Some helpful methods that can be called from within commands are:
* `Display.render` - Forces a refresh of the console
* `Display.clear` - Clears the console (system command: cls)
* `Display.title` - Formatting, creates a title
* `UserInput.setInput` - Can be used to set a default value from the user

## Styling and media
Colouring can be achieved through the four CSS classes:
* blue
* red
* green
* yellow
on a `<span>` element.

Tables have automatic formatting, including yellow highlighting of `<th>` elements.

HTML output is permitted into the console, and this can be used to insert images. For stylistic reasons, these images should be precomposed to 16 colours without dithering. More information about image formatting can be found at /images/profile/readme.txt.

# Roadmap
* Allow methods to capture user input during their execution (aka prompts)
* Sound
* Animation