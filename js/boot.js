var bootLines = [
  "Award Modular BIOS v6.00PG, An Energy Star Ally",
  "Copyright (C) 1984-99, Award Software Inc.",
  "<br/>",
  "Main Processor : PENTIUM II 910MHz",
  "Memory Testing : 131072K OK + 1024K Shared Memory",
  "<br/>",
  "Award Plug and Play BIOS Extension v1.0A",
  "Copyright (C) 1999, Award Software Inc.",
  "<br/>",
  "<span class='green'>Trend ChipAwayVirus(R) On Guard Ver 1.64</span>",
  "Tip: type 'help' to see list of available commands"
];

function bootAnimation(){
  Display.appendLine('<img src="images/energy-star.png" style="float:right"/>');
  var delay = 0;
  for(var i=0; i<bootLines.length; i++){
    delay += Math.random()*200;
    setTimeout(function(i){
      Display.appendLine(bootLines[i]);
    },delay,i);
  }
}