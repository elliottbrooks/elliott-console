// Caret

function caretAnimate(state){
  function setElsVisibility(els,vis){
    [].forEach.call(els, function(el){
      el.style.visibility = vis;
    });
  }

  var els = document.querySelectorAll("#user-input-caret,blink");

  if(state){
    setElsVisibility(els,"hidden");
  }else{
    if(document.hasFocus()){
      setElsVisibility(els,"visible");
    };
  }
  var new_state = !state;
  setTimeout(function(){
    caretAnimate(new_state);
  },500);
}