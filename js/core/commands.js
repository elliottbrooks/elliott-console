// Commands

var Commands = {};
Commands.registered_commands = {};

Commands.register = function(obj){
  if(!obj.name){
    throw "No command name specified";
    return;
  }

  if(Commands.registered_commands[obj.name]){
    throw "Duplicate command registered";
    return;
  }

  Commands.registered_commands[obj.name] = obj;
}

Commands.executeCommand = function(command){
  var segments = command.match(/(\"|')([A-z0-9 ]+)(\"|')|[\S]+/g);
  var command_match = Commands.registered_commands[segments[0]];
  if(command_match){
    command_match.execute(segments,Display.appendLine);
    History.lastCommand = command_match;
  }else{
    Display.appendLine('<span class="red">Error</span>: no command '+segments[0]+' found');
  }
}

Commands.exists = function(commandname){
  return !!Commands.registered_commands[commandname];
}

Commands.getCommandsByName = function(input){
  var keys = Object.keys(Commands.registered_commands);
  var matches = [];
  for(var i=0; i<keys.length; i++){
    if(keys[i].slice(0,input.length)==input){
      matches.push(keys[i]);
    }
  }
  return matches;
}

Commands.autocomplete = function(partial_command){
  var segments = partial_command.split(' ');
  if(segments.length==1){
    var matches = Commands.getCommandsByName(segments[0]);
    if(matches.length==1){
      return matches[0];
    }else{
      return null;
    }
  }else{
    if(Commands.exists(segments[0])){
      var command_match = Commands.registered_commands[segments[0]];
      if(command_match && command_match.autocomplete){
        return command_match.autocomplete(segments);
      }
    }
  }
}
