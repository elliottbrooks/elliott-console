// Display
var Display = {};

Display.lines = [];

Display.rewriteLine = function(string){
  Display.lines[Display.lines.length-1] = string;
  Display.render();
}

Display.appendLine = function(string){
  Display.lines.push(string);
  Display.render();
}

Display.getLines = function(){
  return Display.lines;
}

Display.render = function(){
  var el = document.querySelector('#display');
  var content = "";
  var lines = Display.getLines();
  for(var i=0; i<lines.length; i++){
    content += ('<div>'+lines[i]+'</div>\n\n');
  }
  el.innerHTML = content;
  el.scrollTop = el.scrollHeight;
}

Display.clear = function(){
  Display.lines = [];
  Display.render();
}

Display.title = function(string){
  return '==='+string+'===';
}

Display.progressBar = function(amt,total,width){
  if(!width) width = 50;
  var progress = amt*width/total;
  var toreturn = "[";
  for(var i=0; i<width; i++){
    toreturn += (i<=progress) ? '<span class="blue">|</span>' : '-';
  }
  return toreturn+"]";
}

Display.ajaxWrapper = function(networkEvent,string){
  var keepRendering = true;
  var currentFrame = 0;

  function renderFrame(int){
    var outerWidth = 40;
    var innerWidth = 10;
    var output = (string ? string : 'Loading...') + " [";
    for(var i=0; i<outerWidth; i++){
      var current = false;
      for(var j=0; j<innerWidth; j++){
        if(i==((int+j)%outerWidth)) current = true;
      }
      output += current ? '<span class="blue">|</span>' : '-';
    }
    output += "]";
    Display.rewriteLine(output);
  }

  function renderTimeout(){
    if(!keepRendering){
      return;
    }
    renderFrame(currentFrame);
    currentFrame++;
    setTimeout(function(){
      renderTimeout();
    },50);
  }

  function stopRendering(){
    Display.rewriteLine("Loaded.");
    keepRendering = false;
  }

  var promise = new Promise(function(resolve,reject){
    Display.appendLine('Loading...');
    renderTimeout();
    networkEvent.then(function(data){
      stopRendering();
      resolve(data);
    },function(data){
      stopRendering();
      reject(data);
    })
  });
  return promise;
}