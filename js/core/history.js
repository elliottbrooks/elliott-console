// History

var History = {};

History.lastCommand;
History.commands = [];
History.current_index = -1;
History.custom_command;

History.getCurrentCommand = function(){
  if(History.current_index==-1){
    return History.custom_command;
  }else{
    return History.commands[(History.commands.length-1) - History.current_index];
  }
}

History.browseUp = function(){
  if(History.current_index<(History.commands.length-1)){
    if(History.current_index==-1){
      History.custom_command = UserInput.getInput();
    }
    History.current_index++;
  }
  UserInput.setInput(History.getCurrentCommand());
}

History.browseDown = function(){
  if(History.current_index>-1){
    History.current_index--;
  }
  UserInput.setInput(History.getCurrentCommand());
}

History.preserveCommand = function(command){
  if(History.commands[History.commands.length-1]!=command){
    History.commands.push(command);
  }
}

History.reset = function(){
  History.custom_command = "";
  History.current_index = -1;
  UserInput.setInput(History.getCurrentCommand());
}