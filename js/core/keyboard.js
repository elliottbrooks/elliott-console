// Keyboard

function handleKeyPress(event){
  var char = String.fromCharCode(event.charCode);
  UserInput.append(char);
}

function handleKeyDown(event){
  var code = event.keyCode;

  var lastCommand = History.lastCommand;
  if(lastCommand && lastCommand.handleInput && lastCommand.handleInput(code)) {
  } else {
    switch(classifyKeyCode(code)){
      case 'alphanum':
        return true;
      case 'space':
        handleKeyPress({charCode:32});
        break;
      case 'backspace':
        UserInput.backspace();
        break;
      case 'tab':
        UserInput.autocomplete();
        break;
      case 'enter':
        UserInput.execute();
        break;
      case 'arrow_up':
        History.browseUp();
        break;
      case 'arrow_down':
        History.browseDown();
        break;
    }
  }

  event.preventDefault();
  return false;
}

function classifyKeyCode(code){
  if((code>=48&&code<=90)||code>=186){
    return 'alphanum';
  }
  switch(code){
    case 8:
      return 'backspace';
    case 9:
      return 'tab';
    case 13:
      return 'enter';
    case 16:
      return 'shift';
    case 32:
      return 'space';
    case 38:
      return 'arrow_up';
    case 40:
      return 'arrow_down';
    default:
      return null;
  }
}