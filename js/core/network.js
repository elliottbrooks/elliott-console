var Network = {};

Network.get = function(url){
  var promise = new Promise(function(resolve,reject){

    var request = new XMLHttpRequest();
    request.open('GET', url, true);

    request.onload = function() {
      if (request.status >= 200 && request.status < 400) {
        resolve(request.responseText);
      } else {
        reject(request.responseText);
      }
    };

    request.onerror = function() {
     reject('Connection Error');
    };

    request.send();
  });

  return promise;
}

Network.post = function(url,data){
  var promise = new Promise(function(resolve,reject){

    var request = new XMLHttpRequest();
    request.open('POST', url, true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

    request.onload = function() {
      if (request.status >= 200 && request.status < 400) {
        resolve(request.responseText);
      } else {
        reject(request.responseText);
      }
    };

    request.onerror = function() {
     reject('Connection Error');
    };

    request.send(data);
  });

  return promise;
}