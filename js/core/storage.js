var Storage = {};

Storage.getAllFiles = function(){
  var files = localStorage.getItem('files');
  if(!files){
    return [];
  }
  return JSON.parse(files);
}

Storage.getFile = function(filename){
  var files = Storage.getAllFiles();
  for(var i=0; i<files.length; i++){
    if(files[i].filename == filename){
      var file = files[i];
      file.index = i;
      return file;
    }
  }
  return null;
}

Storage.readFile = function(filename){
  var file = Storage.getFile(filename)
  if(!file){
    return null;
  }
  
  return file.data;
}

Storage.writeFile = function(filename,data){
  var files = Storage.getAllFiles();

  var currentFile = Storage.getFile(filename);

  if(currentFile){
    currentFile.data = data;
    currentFile.lastModified = new Date();
    files[currentFile.index] = currentFile;
  }else{
    var newfile = {
      filename: filename,
      data: data,
      lastModified: new Date()
    }
    files.push(newfile);
  }

  localStorage.setItem('files',JSON.stringify(files));
}

Storage.deleteFile = function(filename){
  var files = Storage.getAllFiles();
  if(filename=="*"){
    files = [];
  }else{
    var file = Storage.getFile(filename);
    if(!file){
      return null;
    }
    files.splice(file.index,1);
  }
  localStorage.setItem('files',JSON.stringify(files));
  return true;
}