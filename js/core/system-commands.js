// System Commands
Commands.register({
  name:'cls',
  description:'Clear the screen',
  execute:function(input,output){
    Display.clear();
  }
});

Commands.register({
  name:'help',
  description:'Display this list',
  execute:function(input,output){
    output(Display.title("<span class=yellow>Available Commands</span>"));
    output("");
    output("<table><tr><th>Command</th><th>Description/Usage</th></tr>")
    var commands = Object.keys(Commands.registered_commands);
    for(var i=0; i<commands.length; i++){
      var command = Commands.registered_commands[commands[i]];
      output('<tr><td class=yellow>'+command.name+'</td><td>');
      output(command.description);
      output('</td></tr>');
    }
    output("</table>");
  }
});

Commands.register({
  name:'cat',
  description:'Concat file contents.</br>Usage: cat [filename]',
  execute:function(input,output){
    var filename = input[1];
    if(!filename){
      output('<span class="red">No filename specified</span>');
      return;
    }
    var fileContents = Storage.readFile(filename);
    if(!fileContents){
      output('<span class="red">No file found with name '+filename+'</span>');
      return;
    }
    output(fileContents);
  }
})

Commands.register({
  name:'echo',
  description:'Display a line of text. Can pipe output to file. <br/>Usage: echo "Hello" ; echo "Hello" &gt; hello.txt',
  execute:function(input,output){
    var string = input[1];
    if(input[2] == '>'){
      var filename = input[3];
      if(!filename){
        output("<span class='red'>No filename specified</span>");
        return;
      }
      Storage.writeFile(filename,string);
      output('<span class="green">Output written to file '+filename+'</span>');
    }else{
      output(string);
    }
  }
})

Commands.register({
  name:'ls',
  description:'Display any saved files. Does not take a parameter.',
  execute:function(input,output){
    var files = Storage.getAllFiles();
    if(files.length==0){
      output('Directory is empty.');
      return;
    }
    var table = "<table><tr><th>Filename</th><th>Last Modified</th></tr>";
    for(var i=0; i<files.length; i++){
      table += '<tr><td>'+files[i].filename+'</td><td>'+files[i].lastModified.slice(0,19)+'</td></tr>';
    }
    table += '</table>';
    output(table);
  }
})

Commands.register({
  name:'rm',
  description:'Remove a file. Takes a filename or wildcard. No regexes.<br/>Usage: rm hello.txt ; rm *',
  execute:function(input,output){
    var filename = input[1];
    if(!filename){
      output('<span class="red">No filename specified</span>');
      return;
    }
    var deleted = Storage.deleteFile(filename);
    if(!deleted){
      output('<span class="red">No file found: '+filename+'</span>');
      return;
    }
    output('File successfully deleted');
  }
})
