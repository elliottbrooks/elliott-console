
function init(){
  caretAnimate(true);
  bootAnimation();
}

// User Input
var UserInput = {};

UserInput.getElement = function(){
  return document.querySelector('#user-input');
}

UserInput.getInput = function(){
  return UserInput.getElement().textContent;
}

UserInput.setInput = function(string){
  UserInput.getElement().textContent = string;
}

UserInput.append = function(char){
  var new_content = UserInput.getInput() + char;
  UserInput.setInput(new_content);
}

UserInput.backspace = function(){
  var old_content = UserInput.getInput();
  var new_content = old_content.slice(0,(old_content.length-1));
  UserInput.setInput(new_content);
}

UserInput.execute = function(){
  var command = UserInput.getInput();
  if(command.length>0){
    Display.appendLine('> '+command);
    History.preserveCommand(command);
    History.reset();
    Commands.executeCommand(command);
  }
}

UserInput.autocomplete = function(){
  var partial_command = UserInput.getInput();
  if(partial_command.length>0){
    var response = Commands.autocomplete(partial_command);
    if(response){
      UserInput.setInput(response);
    }
  }
}