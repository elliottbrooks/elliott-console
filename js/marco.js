var Marco = {
  name:'marco',
  description:'An example command<br/>Usage: marco [name]',
  execute:function(input,output){
    var reply = "Polo"
    if(input.length>1){
      reply += ', '+input[1];
    }
    reply += '!';
    output(reply);
  },
  handleInput:function(keyCode) {
    var keyMultiplyCode = 106;
    if(keyCode == keyMultiplyCode) {
      alert('Marco is a star!');
      return true;
    }
    return false;
  }
}

Commands.register(Marco);
